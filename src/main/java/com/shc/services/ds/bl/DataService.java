package com.shc.services.ds.bl;

import com.shc.services.ds.api.request.ServiceRequest;
import com.shc.services.ds.api.response.ServiceResponse;

public interface DataService {
    ServiceResponse create(ServiceRequest serviceRequest) throws Exception;
    ServiceResponse read(ServiceRequest serviceRequest) throws Exception;
    ServiceResponse update(ServiceRequest serviceRequest) throws Exception;
    ServiceResponse delete(ServiceRequest serviceRequest) throws Exception;
}
