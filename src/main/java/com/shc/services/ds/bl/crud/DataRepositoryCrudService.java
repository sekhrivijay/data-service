package com.shc.services.ds.bl.crud;


import com.shc.services.ds.api.request.ServiceRequest;
import com.shc.services.ds.api.response.Data;

public interface DataRepositoryCrudService  {
    Data create(ServiceRequest serviceRequest) throws Exception;
    Data read(ServiceRequest serviceRequest) throws Exception;
    Data update(ServiceRequest serviceRequest) throws Exception;
    Data delete(ServiceRequest serviceRequest) throws Exception;
}
