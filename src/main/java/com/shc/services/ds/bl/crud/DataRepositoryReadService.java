package com.shc.services.ds.bl.crud;

import com.shc.services.ds.api.request.ServiceRequest;
import com.shc.services.ds.api.response.Data;

public interface DataRepositoryReadService {
    Data read(ServiceRequest serviceRequest) throws Exception;
}
