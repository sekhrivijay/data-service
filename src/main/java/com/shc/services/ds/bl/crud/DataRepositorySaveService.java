package com.shc.services.ds.bl.crud;


import com.shc.services.ds.api.request.ServiceRequest;
import com.shc.services.ds.api.response.Data;

public interface DataRepositorySaveService {
    Data save(ServiceRequest serviceRequest) throws Exception;
}
