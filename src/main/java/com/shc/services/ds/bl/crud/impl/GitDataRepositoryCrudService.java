package com.shc.services.ds.bl.crud.impl;

import com.shc.services.ds.api.request.ServiceRequest;
import com.shc.services.ds.api.response.Data;
import com.shc.services.ds.bl.crud.DataRepositoryCrudService;

import javax.inject.Named;

@Named
public class GitDataRepositoryCrudService extends DataRepositoryCrudServiceBase {
    @Override
    public Data read(ServiceRequest serviceRequest) throws Exception {
        return new Data("This is a test ".getBytes());
    }

    @Override
    public Data create(ServiceRequest serviceRequest) throws Exception {
        return null;
    }
}
