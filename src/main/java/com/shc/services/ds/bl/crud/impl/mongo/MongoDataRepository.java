package com.shc.services.ds.bl.crud.impl.mongo;


import com.shc.services.ds.api.response.ServiceResponse;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MongoDataRepository extends MongoRepository<ServiceResponse, String> {
}
