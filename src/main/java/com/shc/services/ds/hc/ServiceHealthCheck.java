package com.shc.services.ds.hc;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class ServiceHealthCheck implements HealthIndicator {

//    @Inject
//    private HelloBusinessLogic helloBusinessLogic;

    @Override
    public Health health() {
        return Health.up().build();
    }
}